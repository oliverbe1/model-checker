Mmc is a toy model checker written in C. I wrote it 
purely to learn stuff. If you're interested in robust model checking tools, 
have a look at 'NuSMV' or 'SPIN'.

Build it using cmake. On linux: `(cd build; cmake .. && make)`.
See the example below for information on how to use it.

## Model Checking ##

Model checking is a verification technique where you construct 
an abstract model of your system and state certain properties 
that must hold in this model. For example:

- Deadlocks can never occur.
- All user requests are eventually fulfilled.
- The oven can only heat up when the door is closed.

The model is typically a form of finite state machine, where 
a path through the machine represents a run of your system.
Properties are written in a formal logic able to 
make statements about computation paths of your model.
CTL (Computation Tree Logic) is the logic used in mmc.

A model checker verifies the properties for you. If one's
invalid, a run of the system violating the property is shown.

## Example: The Ferryman Puzzle ##

In this example we'll use mmc to find a solution to the 
ferryman puzzle. A ferryman needs to carry a goat, a wolf 
and some cabbage across a river. Trouble is, he can carry 
at most one of them at a time. If he leaves the goat alone 
with the cabbage, the goat will eat it. If the wolf is left 
alone with the goat, he'll eat the goat. How does the 
ferryman get all of them across without anyone being harmed?

```
                       [f] (g) (c) (w)
 ---------------       ---------------
   ~   ~   ~   ~   ->    ~   ~   ~   ~
 ---------------       ---------------
 [f] (g) (c) (w)       
```

### States ###

Mmc has its own file format to describe a model and its 
properties. The states of the model are described by a number of boolean 
variables. We'll need to keep track of which side the ferryman,
goat, wolf & cabbage are on, and which one of them is being 
carried.

```
var ferryman      = 0   // side of the riverbank
var goat          = 0
var cabbage       = 0
var wolf          = 0

var carry_goat    = 0   // thing being carried
var carry_cabbage = 0
var carry_wolf    = 0
```

Variables must be initialized, defining the initial state of the model.

### Transitions ###

By default, any state can reach any other state. We can restrict 
the valid transitions by stating various constraints as propositional 
logic formulae. Variable values after a transition are referred to
as `next(variable)`.

The ferryman can carry at most one passenger. Which is the same as saying 
he can never carry any two simultaneously:

```
!(next(carry_goat)    ^ next(carry_cabbage))
!(next(carry_cabbage) ^ next(carry_wolf)   )
!(next(carry_wolf)    ^ next(carry_goat)   )
```

Here, `!` means 'not', `^` means 'and'. Furthermore, the ferryman can 
choose to carry something only if he's on the same side of the riverbank:

```
next(carry_goat) -> ferryman = goat
```

The symbol `->` can be read as 'only if', but also as 'if .. then ..'.
The latter interpretation is useful to specify that if the goat 
is chosen, he'll be on the same side of the riverbank as the ferryman:

```
next(carry_goat) -> next(goat) = next(ferryman)
```

And finally, and easy to forget, the goat can't swim across by himself:

```
!next(carry_goat) -> next(goat) = goat
```

The last 3 constraints are repeated for the wolf and the cabbage.

### Properties ###

The puzzle asks if there's some series of steps that gets
all across without harm. No one is harmed if the ferryman 
is with the goat whenever the goat is with the cabbage, 
or the wolf with the goat:

```
check E[ goat = cabbage v goat = wolf -> goat = ferryman
         U
         cabbage ^ goat ^ wolf ^ ferryman ]
```

`E[ p U q ]` is a CTL formula that states there **E**xists 
some path where `p` holds **U**ntil `q` holds, and that 
`q` does eventually hold. 

If a property is true, mmc tries to find a satisfying path,
otherwise it looks for a path that refutes it. Not all CTL 
properties can be confirmed or refuted with a single path.

### Full Program ###

```
// state variables

var ferryman      = 0
var goat          = 0
var cabbage       = 0
var wolf          = 0

var carry_goat    = 0
var carry_cabbage = 0
var carry_wolf    = 0

// transition constraints

!(next(carry_goat)    ^ next(carry_cabbage))
!(next(carry_cabbage) ^ next(carry_wolf)   )
!(next(carry_wolf)    ^ next(carry_goat)   )

 next(carry_goat) -> ferryman = goat
 next(carry_goat) -> next(goat) = next(ferryman)
!next(carry_goat) -> next(goat) = goat

 next(carry_cabbage) -> ferryman = cabbage
 next(carry_cabbage) -> next(cabbage) = next(ferryman)
!next(carry_cabbage) -> next(cabbage) = cabbage

 next(carry_wolf) -> ferryman = wolf
 next(carry_wolf) -> next(wolf) = next(ferryman)
!next(carry_wolf) -> next(wolf) = wolf

// properties

check E[ goat = cabbage v goat = wolf -> goat = ferryman
         U
         cabbage ^ goat ^ wolf ^ ferryman ]
```

### Output ###

```
$ build/mmc examples/ferryman.mmc

== mmc v0.1 - a modest model checker
== reading examples/ferryman.mmc ...
== verifying model ...
== ok. 40 states, 224 transitions.

check 'E[ goat = cabbage v goat  ..'
True. Witness found:

  [State 0]

  [State 1]
    ferryman
    goat
    carry_goat

  [State 2]
    goat

  [State 3]
    ferryman
    goat
    cabbage
    carry_cabbage

  [State 4]
    cabbage
    carry_goat

  [State 5]
    ferryman
    cabbage
    wolf
    carry_wolf

  [State 6]
    cabbage
    wolf

  [State 7]
    ferryman
    goat
    cabbage
    wolf
    carry_goat

  ...

== model saved.
```

