mmc - File Format Grammar
=========================

```
program = declaration+ constraint* property*

declaration = 'var' identifier '=' ('0' | '1')

constraint = ctl_expr
(only the propositional subset of ctl_expr)

property = 'check' ctl_expr
(but use of next(..) is not allowed here)

identifier = [a-zA-Z0-9_] [a-zA-Z0-9_.]*

ctl_expr          = ctl_equiv_expr
ctl_equiv_expr    = ctl_implies_expr (('<->'|'<=>') ctl_implies_expr)*
ctl_implies_expr  = ctl_rimplies_expr(('->'|'=>') ctl_rimplies_expr)*
ctl_rimplies_expr = ctl_or_expr      (('<-'|'<=') ctl_or_expr)*
ctl_or_expr       = ctl_xor_expr     (('v'|'|') ctl_xor_expr)*
ctl_xor_expr      = ctl_and_expr     ('(+)' ctl_and_expr)*
ctl_and_expr      = ctl_eq_expr      (('^'|'&') ctl_eq_expr)*
ctl_eq_expr       = ctl_neq_expr     ('=' ctl_neq_expr)*
ctl_neq_expr      = ctl_not_expr     ('!=' ctl_not_expr)*
ctl_unary_expr    = ('!'|'AF'|'EF'|'AX'|'EX'|'AG'|'EG') ctl_unary_expr 
                  | ctl_until_expr
ctl_until_expr    = 'A[' ctl_expr 'U' ctl_expr ']'
                  | 'E[' ctl_expr 'U' ctl_expr ']'
                  | ctl_atomic_expr
ctl_atomic_expr   = identifier | 'next(' identifier ')'
                  | '(' ctl_expr ')' 
                  | 0 | 1
```

Files may include line comments '// ...'.
