﻿/* executable entrypoint, basic glue, output */

#include "ctype.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "arraylist.h"
#include "bdd.h"
#include "check.h"
#include "model.h"
#include "parser.h"

static int verbose;
static void print_usage ();
static MmcFile read_file_with_name (const char* fname);
static void verify_model (Model* m);
static void check_property (Model* m, Property* p);
static void save_model (Model* m, const char* fname);

/*
======================================================================
  Main
======================================================================
*/

int main (int argc, char** argv)
{
   int i;
   MmcFile f;
   const char* fname;

   if (argc < 2) print_usage();
   verbose = !strcmp(argv[1], "-v");
   if (verbose && argc < 3) print_usage();
   fname = verbose ? argv[2] : argv[1];

   printf("== mmc v0.1 - a modest model checker\n");
   printf("== reading %s ...\n", fname);
   f = read_file_with_name(fname);
   if (verbose)
      save_model(f.model, "model.dot");
   verify_model(f.model);
   for (i = 0; i < arraylist_size(f.properties); ++i)
      check_property(f.model, arraylist_get(f.properties, i));

   model_free(f.model);  /* cleanup */
   arraylist_free(f.properties);
   bdd_init(0);
}

/*
======================================================================
*/

static void print_usage ()
{
   printf("Usage: mmc [-v] FILE\nVerbose mode '-v' counts model "
         "states & transitions. It also saves\nyour model as a "
         "graphviz dot file. But it is slow for large models.\n");
   exit(1);
}

static MmcFile read_file_with_name (const char* fname)
{
   MmcFile f;
   FILE* fp;

   fp = fopen(fname, "r");
   if (!fp) {
      printf("could not find or open file\n");
      exit(1);
   }
   f = mmc_read_file(fp);
   fclose(fp);
   return f;
}

static void model_error (const char* msg)
{
   printf("invalid model: %s\n", msg);
   exit(1);
}

static void verify_model (Model* m)
{
   Bdd R = model_relation(m);
   Bdd I = model_initial_states(m);
   Bdd reachable = model_reachable(m, I);
   Bdd pre = bdd_and(reachable, model_predecessors(m, reachable));
   Bdd reachable_r = model_set_to_relation(m, reachable);

   printf("== verifying model ...\n");

   if (R == bdd_false())
      model_error("there are no transitions satisfying your constraints. "
                  "(do some of your constraints contradict each other?)");
   if (reachable != pre)
      model_error("not all reachable states have a transition. "
                  "(did you specify constraints for current states that "
                  "aren't required for next states?)");
   if (verbose)
      printf("== ok. %ld states, %ld transitions.\n\n", 
            bdd_satcount(reachable_r), bdd_satcount(bdd_and(R, reachable)));
   else
      printf("== ok.\n\n");
}

static void indent (int n)
{
   int i;
   for (i = 0; i < n; ++i)
      printf(" ");
}

static void report_path (Model* m, Path* p)
{
   int i, j, v;

   for (i = 0; i < p->num_states; ++i) {
      if (i == p->loop_start)
         printf("Loop starts here:\n\n");
      indent(i < p->loop_start ? 0 : 2);
      printf("[State %d]\n", i);
      for (j = 0; j < p->num_vars; ++j) {
         v = p->values[i*p->num_vars + j];
         if (v) {
            indent(i < p->loop_start ? 2 : 4);
            printf("%s\n", model_var_name(m, j));
         }
      }
      printf("\n");
   }
   printf("  ...\n\n");
}

static void print_truncated (const char* s)
{
   int i = 0;
   while (*s && i++ < 25) {
      if (!isspace(*s) || (*s == ' ' && *(s+1) != ' '))
         printf("%c", *s);
      s++;
   }
}

static void check_property (Model* m, Property* p)
{
   int i;
   Bdd I = model_initial_states(m);

   printf("check '"); print_truncated(p->text); printf(" ..'\n");
   Bdd bdd = mmc_check(m, p->formula);
   if (bdd_and(bdd, I) == I) {
      Path* path = mmc_witness(m, p->formula);
      if (path) {
         printf("True. Witness found:\n\n");
         report_path(m, path);
         path_free(path);
      } else {
         printf("True.\n\n");
      }
   } else {
      Path* path = mmc_counter_example(m, p->formula);
      if (path) {
         printf("False. Counterexample found:\n\n");
         report_path(m, path);
         path_free(path);
      } else {
         printf("False.\n\n");
      }
   }
}

static void save_model (Model* m, const char* fname)
{
   FILE* fp = fopen(fname, "w");
   model_export_dot(m, fp);
   fclose(fp);
   printf("== saving model ...\n");
}
