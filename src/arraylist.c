#include "assert.h"
#include "stdlib.h"
#include "string.h"
#include "arraylist.h"

#define INITIALSIZE  10
#define GROWTHFACTOR 2

struct ArrayListTag 
{
   T* data;         /* ptr to start of elements */
   int sz, max;     /* size of list & buffer respectively */
   FreeFn freefn;   /* function called when elements are removed */
};

static void grow_buffer (ArrayList a)
{
   a->max = a->max ? GROWTHFACTOR*a->max : INITIALSIZE;
   a->data = realloc(a->data, a->max * sizeof(T));
}

/* construction/destruction */

ArrayList arraylist_new ()
{
   return calloc(1, sizeof(struct ArrayListTag));
}

ArrayList arraylist_with_ownership (FreeFn freefn)
{
   ArrayList a = arraylist_new();
   a->freefn = freefn;
   return a;
}

void arraylist_free (ArrayList a)
{
   arraylist_resize(a, 0);  /* destroy elements if needed */
   free(a->data);
   free(a);
}

/* ownership */

void arraylist_release_ownership (ArrayList a)
{
   a->freefn = 0;
}

/* accessors */

T arraylist_get (ArrayList a, int i)
{
   assert( a );
   assert( i >= 0 && i < arraylist_size(a) );
   return a->data[i];
}

T arraylist_back (ArrayList a)
{
   assert( a );
   assert( !arraylist_is_empty(a) );
   return a->data[a->sz - 1];
}

int arraylist_size (ArrayList a)
{
   assert( a );
   return a->sz;
}

int arraylist_is_empty (ArrayList a)
{
   assert( a );
   return arraylist_size(a) == 0;
}

/* mutators */

void arraylist_set (ArrayList a, int i, T val)
{
   assert( a );
   assert( i >= 0 && i < arraylist_size(a) );
   if (a->freefn)
      a->freefn(a->data[i]);
   a->data[i] = val;
}

void arraylist_push (ArrayList a, T elem)
{
   assert( a );
   if (a->sz == a->max)
      grow_buffer(a);
   a->data[a->sz++] = elem;
}

T arraylist_pop (ArrayList a)
{
   assert( a );
   T back = arraylist_back(a);
   arraylist_resize(a, a->sz - 1);
   return back;
}

void arraylist_resize (ArrayList a, int sz_new)
{
   T* el;

   assert( a );
   assert( sz_new >= 0 );
   if (sz_new > a->sz) {
      while (a->max < sz_new)
         grow_buffer(a);
      memset(a->data + a->sz, 0, (sz_new - a->sz) * sizeof(T));
   } else if (a->freefn) {
      for (el = a->data + sz_new; el != a->data + a->sz; ++el)
         a->freefn(*el);
   }
   a->sz = sz_new;
}

void arraylist_append (ArrayList a, ArrayList b)
{
   arraylist_append_data(a, b->data, b->sz);
}

void arraylist_append_data (ArrayList a, T* data, int n)
{
   while ((a->max) < (a->sz + n))
      grow_buffer(a);
   memcpy(a->data + a->sz, data, n * sizeof(T));
   a->sz += n;
}

/* unsafe */

T* arraylist_data (ArrayList a)
{
   return a->data;
}
