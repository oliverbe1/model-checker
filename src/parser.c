/* Lexing, parsing & semantic analysis are done in 1 pass. GL. */

#include "ctype.h"
#include "stdlib.h"
#include "string.h"
#include "parser.h"

#define MAXVARS    200
#define MAXCHECKS  100

typedef const char* Cursor;

/*
======================================================================
  Parser State        
======================================================================
*/

typedef struct Parser
{
   const char* input;            /* the input string */

   int   num_vars;               /* declared boolean variables */
   char* var_name [MAXVARS];
   int   var_value[MAXVARS];

   Ctl*  constraints;            /* transition constraints so far */

   int   num_checks;             /* properties of model to be checked */
   char* check_str[MAXCHECKS];  
   Ctl*  check_ctl[MAXCHECKS];  
   
   enum { STATE_DECL, STATE_CONS, STATE_CHCK } state;
} 
Parser;

static void parse_error (Parser*, Cursor, const char*);

static Parser* parser_new (const char* input)
{
   Parser* p = malloc(sizeof(Parser));

   p->input = input;
   p->num_vars = 0;
   p->constraints = ctl_true();
   p->num_checks = 0;
   p->state = STATE_DECL;
   return p;
}

static int var_id (Parser* p, Cursor s, Cursor e)
{
   int i;
   for (i = 0; i < p->num_vars; ++i)
      if (strlen(p->var_name[i]) == (e-s) && 
            !strncmp(p->var_name[i], s, (e-s)))
         return i;
   return -1;
}

static int add_var (Parser* p, Cursor s, Cursor e, int val)
{
   char* name;

   if (p->num_vars == MAXVARS)
      parse_error(p, s, "maximum number of variables exceeded");
   name = malloc(e - s + 1);
   strncpy(name, s, (e - s));
   name[e - s] = '\0';
   p->var_name[p->num_vars] = name;
   p->var_value[p->num_vars] = val;
   p->num_vars++;
   return p->num_vars - 1;
}

/*
======================================================================
  Error Reporting
======================================================================
*/

typedef struct LineInfo
{
   int line;  /* both 1-based */
   int col;
}
LineInfo;

LineInfo line_info (const char* input, Cursor pos)
{
   LineInfo info = { 1, 0 };
   Cursor c = input;

   while (c < pos)
      if (*c++ == '\n')
         info.line++;
   while (c >= input && *c-- != '\n')
      info.col++;
   return info;
}

static void parse_error (Parser* p, Cursor c, const char* msg)
{
   LineInfo info = line_info(p->input, c);
   printf("error reading file (line %d col %d): %s\n", info.line, info.col, msg);
   exit(1);
}

/*
======================================================================
  Misc. Lexing Helpers 
======================================================================
*/

static Cursor skip_line (Cursor c)
{
   while (*c != '\n')
      c++;
   return c + 1;
}

static Cursor skip_ws (Cursor c)
{
   while (isspace(*c))
      c++;
   if (*c == '/' && *(c + 1) == '/')
      return skip_ws(skip_line(c));
   return c;
}

static Cursor match (Cursor c, const char* s)
{
   while (*c && *c == *s)
      c++, s++;
   return (*s == '\0') ? c : 0;
}

static Cursor expect (Parser* p, Cursor c, const char* s)
{
   Cursor res = match(c, s);
   if (!res)
      parse_error(p, c, "unexpected input");
   return res;
}

static Cursor identifier (Cursor c) /* TODO: check keyword clashes */
{
   if (!isalpha(*c) && *c != '_')
      return 0;
   c++;
   while (isalnum(*c) || *c == '_' || *c == '.')
      c++;
   return c;
}

/*
======================================================================
  Grammar Rules
======================================================================
*/

static Cursor program (Parser* p, Cursor c);
static Cursor decl (Parser* p, Cursor c);
static Cursor constraint (Parser* p, Cursor c);
static Cursor property (Parser* p, Cursor c);
static Cursor ctl_expr (Parser* p, Cursor c, Ctl** out);
static Cursor ctl_equiv_expr (Parser* p, Cursor c, Ctl** out);
static Cursor ctl_implies_expr (Parser* p, Cursor c, Ctl** out);
static Cursor ctl_rimplies_expr (Parser* p, Cursor c, Ctl** out);
static Cursor ctl_or_expr (Parser* p, Cursor c, Ctl** out);
static Cursor ctl_xor_expr (Parser* p, Cursor c, Ctl** out);
static Cursor ctl_and_expr (Parser* p, Cursor c, Ctl** out);
static Cursor ctl_eq_expr (Parser* p, Cursor c, Ctl** out);
static Cursor ctl_neq_expr (Parser* p, Cursor c, Ctl** out);
static Cursor ctl_unary_expr (Parser* p, Cursor c, Ctl** out);
static Cursor ctl_until_expr (Parser* p, Cursor c, Ctl** out);
static Cursor ctl_atomic_expr (Parser* p, Cursor c, Ctl** out);
static Cursor ctl_var_expr (Parser* p, Cursor c, Ctl** out);

static Cursor program (Parser* p, Cursor c)
{
   c = skip_ws(c);
   do c = decl(p, c);
   while (match(c, "var"));
   p->state = STATE_CONS;
   while (!match(c, "check") && *c)
      c = constraint(p, c);
   p->state = STATE_CHCK;
   while (match(c, "check"))
      c = property(p, c);
   if (*c) 
      parse_error(p, c, "unexpected input");
   return c;
}

static Cursor decl (Parser* p, Cursor c)
{
   Cursor s, e;  /* start, end of identifier */

   c = expect(p, c, "var");
   c = s = skip_ws(c);
   if (!(e = identifier(c)))
      parse_error(p, c, "invalid variable identifier");
   if (var_id(p, s, e) != -1)
      parse_error(p, c, "duplicate variable declaration");
   c = skip_ws(e);
   c = expect(p, c, "=");
   c = skip_ws(c);
   if (*c == '0')
      add_var(p, s, e, 0);
   else if (*c == '1')
      add_var(p, s, e, 1);
   else
      parse_error(p, c, "unexpected input");
   return skip_ws(c + 1);
}

static Cursor constraint (Parser* p, Cursor c)
{
   Ctl* e;
   Cursor res;

   res = ctl_expr(p, c, &e);
   if (!ctl_is_propositional(e))
      parse_error(p, c, "constraints may not contain temporal connectives.");
   p->constraints = ctl_and(p->constraints, e);
   return res;
}

static Cursor property (Parser* p, Cursor c)
{
   Ctl* e;
   char* str;
   Cursor res;

   if (p->num_checks == MAXCHECKS)
      parse_error(p, c, "maximum number of checks exceeded");
   c = expect(p, c, "check");
   c = skip_ws(c);
   res = ctl_expr(p, c, &e);
   str = malloc(res - c + 1);
   strncpy(str, c, (res - c));
   str[res - c] = '\0';
   p->check_str[p->num_checks] = str;
   p->check_ctl[p->num_checks] = e;
   p->num_checks++;
   return res;
}

/* CTL expressions */

typedef Cursor (*BinaryCtlRule)(Parser* p, Cursor c, Ctl** out);
typedef Ctl*   (*BinaryCtlCtor)(Ctl* lhs, Ctl* rhs);
typedef Ctl*   (*UnaryCtlCtor )(Ctl* e);

static Ctl* ctl_neq (Ctl* lhs, Ctl* rhs)
{
   return ctl_not(ctl_equiv(lhs, rhs));
}
static Ctl* ctl_rimplies (Ctl* lhs, Ctl* rhs)
{
   return ctl_implies(rhs, lhs);
}

static Cursor ctl_expr (Parser* p, Cursor c, Ctl** out)
{
   return ctl_equiv_expr(p, c, out);
}

static Cursor ctl_binary_expr (
      Parser* p, Cursor c, Ctl** out,
      const char* op, const char* alt,
      BinaryCtlRule child_rule, BinaryCtlCtor ctor)
{
   Ctl *e1, *e2;
   Cursor res;

   c = child_rule(p, c, &e1);
   while ((res = match(c, op)) || (alt && (res = match(c, alt)))) {
      c = child_rule(p, skip_ws(res), &e2);
      e1 = ctor(e1, e2);
   }
   *out = e1;
   return c;
}

static Cursor ctl_equiv_expr (Parser* p, Cursor c, Ctl** out)
{
   return ctl_binary_expr(p, c, out, "<->", "<=>", 
         ctl_implies_expr, ctl_equiv);
}

static Cursor ctl_implies_expr (Parser* p, Cursor c, Ctl** out)
{
   return ctl_binary_expr(p, c, out, "->", "=>", 
         ctl_rimplies_expr, ctl_implies);
}

static Cursor ctl_rimplies_expr (Parser* p, Cursor c, Ctl** out)
{
   return ctl_binary_expr(p, c, out, "<-", "<=", 
         ctl_or_expr, ctl_rimplies);
}

static Cursor ctl_or_expr (Parser* p, Cursor c, Ctl** out)
{
   return ctl_binary_expr(p, c, out, "v", "|", 
         ctl_xor_expr, ctl_or);
}

static Cursor ctl_xor_expr (Parser* p, Cursor c, Ctl** out)
{
   return ctl_binary_expr(p, c, out, "(+)", 0, 
         ctl_and_expr, ctl_xor);
}

static Cursor ctl_and_expr (Parser* p, Cursor c, Ctl** out)
{
   return ctl_binary_expr(p, c, out, "^", "&", 
         ctl_eq_expr, ctl_and);
}

static Cursor ctl_eq_expr (Parser* p, Cursor c, Ctl** out)
{
   return ctl_binary_expr(p, c, out, "=", 0, 
         ctl_neq_expr, ctl_equiv);
}

static Cursor ctl_neq_expr (Parser* p, Cursor c, Ctl** out)
{
   return ctl_binary_expr(p, c, out, "!=", 0, 
         ctl_unary_expr, ctl_neq);
}

static Cursor ctl_unary_expr (Parser* p, Cursor c, Ctl** out)
{
   int i;
   Ctl *e;
   Cursor res;

   static const char*  OPS[7]   = 
   { "!",     "AX",   "EX",   "AG",   "EG",   "AF",   "EF"   };
   static UnaryCtlCtor CTORS[7] = 
   { ctl_not, ctl_AX, ctl_EX, ctl_AG, ctl_EG, ctl_AF, ctl_EF };

   for (i = 0; i < 7; ++i) {
      if (res = match(c, OPS[i])) {
         c = ctl_unary_expr(p, skip_ws(res), &e);
         *out = CTORS[i](e);
         return c;
      } 
   }
   return ctl_until_expr(p, c, out);
}

static Cursor ctl_until_expr (Parser* p, Cursor c, Ctl** out)
{
   Ctl *e1, *e2;
   Cursor res;

   if ((res = match(c, "A[")) || (res = match(c, "E["))) {
      c = ctl_expr(p, skip_ws(res), &e1);
      c = skip_ws(expect(p, c, "U"));
      c = ctl_expr(p, skip_ws(c), &e2);
      c = skip_ws(expect(p, c, "]"));
      if (*(res - 2) == 'A')
         *out = ctl_AU(e1, e2);
      else
         *out = ctl_EU(e1, e2);
      return c;
   } else {
      return ctl_atomic_expr(p, c, out);
   }
}

static Cursor ctl_atomic_expr (Parser* p, Cursor c, Ctl** out)
{
   Ctl *e;
   Cursor res;

   if (res = match(c, "0")) {
      *out = ctl_false();
      return skip_ws(res);
   }
   if (res = match(c, "1")) {
      *out = ctl_true();
      return skip_ws(res);
   }
   if (res = match(c, "(")) {
      c = ctl_expr(p, skip_ws(res), out);
      c = expect(p, c, ")");
      return skip_ws(c);
   }
   if (res = match(c, "next")) {
      if (p->state == STATE_CHCK)
         parse_error(p, c, "next() can not be used in checks");
      c = expect(p, skip_ws(res), "(");
      c = ctl_var_expr(p, skip_ws(c), &e);
      c = expect(p, c, ")");
      *out = ctl_var(e->var + 1);
      ctl_free(e);
      return skip_ws(c);
   }
   return ctl_var_expr(p, c, out);
}

static Cursor ctl_var_expr (Parser* p, Cursor c, Ctl** out)
{
   int id;
   Cursor res;

   if (!(res = identifier(c)))
      parse_error(p, c, "invalid variable identifier");
   id = var_id(p, c, res);
   if (id == -1)
      parse_error(p, c, "use of undeclared or invalid variable");
   *out = ctl_var(1 + id*2);
   return skip_ws(res);
}

/*
======================================================================
  API            
======================================================================
*/

static char* read_file (FILE* f)
{
   long sz;
   char* buffer;

   fseek(f, 0, SEEK_END);
   sz = ftell(f);
   buffer = malloc(sz + 1);
   fseek(f, 0, SEEK_SET);
   if (fread(buffer, sz, 1, f) != 1)
      perror("fread");
   buffer[sz] = 0;
   return buffer;
}

static Property* property_new (Ctl* formula, char* text)
{
   Property* prop = malloc(sizeof(Property));

   prop->formula = formula;
   prop->text = text;
   return prop;
}

static void property_free (void* o)
{
   Property* prop = (Property*)o;

   ctl_free(prop->formula);
   free(prop->text);
   free(prop);
}

static Bdd initial_state_bdd (Parser* p)
{
   int i;
   Bdd I;

   Ctl* init = ctl_true();
   for (i = 0; i < p->num_vars; ++i) 
      init = ctl_and(init, p->var_value[i] ?  
            ctl_var(1 + 2*i) : 
            ctl_not(ctl_var(1 + 2*i)));
   I = ctl_as_bdd(init);
   ctl_free(init);
   return I;
}

MmcFile mmc_read_file (FILE* f)
{
   MmcFile res;
   char* s;
   
   s = read_file(f);
   res = mmc_read_string(s);
   free(s);
   return res;
}

MmcFile mmc_read_string (const char* str)
{
   int        i;
   Parser*    p;
   Bdd        R, I;
   ArrayList  names, props;
   MmcFile    mmc;

   /* parse mmc program */
   p = parser_new(str);
   program(p, str);

   /* initialize bdd system */
   bdd_init(p->num_vars * 2);

   /* construct property list */
   props = arraylist_with_ownership(property_free);
   for (i = 0; i < p->num_checks; ++i)
      arraylist_push(props, property_new(p->check_ctl[i], 
               p->check_str[i]));

   /* construct variable names list */
   names = arraylist_with_ownership(free);
   arraylist_append_data(names, (void**)p->var_name, p->num_vars);

   /* build model & mmc format */
   R = ctl_as_bdd(p->constraints);
   I = initial_state_bdd(p);
   mmc.model = model_new(R, I, names);
   mmc.properties = props;

   ctl_free(p->constraints); free(p);
   return mmc;
}
