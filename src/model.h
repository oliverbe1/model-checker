/*
======================================================================

  Model construction, traversal & visualization.

  A model consists of states and a transition relation between 
  states. Both are represented as boolean functions, using BDDs.

  A transition relation R has a transition between state (x1,x2) and 
  (y1,y2) iff R(x1,y1,x2,y2) == 1. Note the interleaving of variables 
  in the BDD order, this is required for performance reasons.

  A set I contains state (x1,x2) iff I(x1,*,x2,*) == 1.

  Only a set of initial states needs to be provided.

======================================================================
*/

#pragma once

#include "stdio.h"
#include "arraylist.h"
#include "bdd.h"

typedef struct Model Model;

/* construction/destruction */

Model* model_new (Bdd R, Bdd I, ArrayList names);

void model_free (Model* m);

/* accessors */

Bdd model_relation (Model* m);

Bdd model_initial_states (Model* m);

const char* model_var_name (Model* m, int var);

int model_num_vars (Model* m);

/* returns the successor states of `S` */

Bdd model_successors (Model* m, Bdd S);

/* returns the states with at least one successor in `S` */

Bdd model_predecessors (Model* m, Bdd S);

/* returns the states whose successors are *all* in `S` */

Bdd model_strict_predecessors (Model* m, Bdd S);

/* converts set to a relation where members are own successors */

Bdd model_set_to_relation (Model* m, Bdd S);

/* returns all states reachable from `I`, in any number of steps */

Bdd model_reachable (Model* m, Bdd I);

/* saves the model in graphviz' dot format */

int model_export_dot (Model* m, FILE* out);
