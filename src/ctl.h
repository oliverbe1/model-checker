/* Computation Tree Logic formulas w/ minimal set of operators */

#pragma once

#include "bdd.h"

#define CTL_TEG   1
#define CTL_TEU   2
#define CTL_TEX   3

#define CTL_TNOT  4
#define CTL_TAND  5
#define CTL_TOR   6

#define CTL_TVAR  7
#define CTL_TTRUE 8

typedef struct Ctl Ctl;

struct Ctl
{
   int  type;   /* type of expression */
   int  var;    /* propositional variable id */
   Ctl* lhs;    /* unary operand or lhs of binary operator */
   Ctl* rhs;    /* rhs of binary operator */

   int  rc;     /* reference count for ctl_free */
                /* TODO: maybe hide struct, it may not be mutated */
};

/* base constructors */

Ctl* ctl_EG (Ctl* e);

Ctl* ctl_EU (Ctl* lhs, Ctl* rhs);

Ctl* ctl_EX (Ctl* e);

Ctl* ctl_not (Ctl* e);

Ctl* ctl_and (Ctl* lhs, Ctl* rhs);

Ctl* ctl_or (Ctl* lhs, Ctl* rhs);

Ctl* ctl_var (int i);

Ctl* ctl_true ();

/* convenience constructors */

Ctl* ctl_EF (Ctl* e);

Ctl* ctl_AG (Ctl* e);

Ctl* ctl_AU (Ctl* lhs, Ctl* rhs);

Ctl* ctl_AX (Ctl* e);

Ctl* ctl_AF (Ctl* e);

Ctl* ctl_xor (Ctl* lhs, Ctl* rhs);

Ctl* ctl_implies (Ctl* lhs, Ctl* rhs);

Ctl* ctl_equiv (Ctl* lhs, Ctl* rhs);

Ctl* ctl_false ();

/* destructor. also destroys all subformulas */

void ctl_free (Ctl* e);

/* formula properties */

int ctl_is_propositional (Ctl* e);

/* convert propositional formula to bdd, assumes bdd_init was called */

Bdd ctl_as_bdd (Ctl* e);

/* debugging */

void ctl_print (Ctl* e);
