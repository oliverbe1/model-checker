/*
======================================================================

  Parser for the .mmc file format.

  Mmc files describe a model and properties of the model to be 
  verified. Model states are described by a tuple of boolean 
  variables, declared and initialized at the start of the file.

  Transitions are implicitly specified by constraining the set 
  of reachable states and next-state variable assignments.
  Constraints take the form of propositional logic formulae.

  Properties are written in Computation Tree Logic (CTL).

  --

  Complete grammar of the file format:

  > program = declaration+ constraint* property*
  
  > declaration = 'var' identifier '=' ('0' | '1')
  
  > constraint = ctl_expr
  > (only the propositional subset of ctl_expr)
  
  > property = 'check' ctl_expr
  > (but use of next(..) is not allowed here)
  >
  > identifier = [a-zA-Z0-9_] [a-zA-Z0-9_.]*
  >
  > ctl_expr          = ctl_equiv_expr
  > ctl_equiv_expr    = ctl_implies_expr (('<->'|'<=>') ctl_implies_expr)*
  > ctl_implies_expr  = ctl_rimplies_expr(('->'|'=>') ctl_rimplies_expr)*
  > ctl_rimplies_expr = ctl_or_expr      (('<-'|'<=') ctl_or_expr)*
  > ctl_or_expr       = ctl_xor_expr     (('v'|'|') ctl_xor_expr)*
  > ctl_xor_expr      = ctl_and_expr     ('(+)' ctl_and_expr)*
  > ctl_and_expr      = ctl_eq_expr      (('^'|'&') ctl_eq_expr)*
  > ctl_eq_expr       = ctl_neq_expr     ('=' ctl_neq_expr)*
  > ctl_neq_expr      = ctl_not_expr     ('!=' ctl_not_expr)*
  > ctl_unary_expr    = ('!'|'AF'|'EF'|'AX'|'EX'|'AG'|'EG') ctl_unary_expr 
  >                   | ctl_until_expr
  > ctl_until_expr    = 'A[' ctl_expr 'U' ctl_expr ']'
  >                   | 'E[' ctl_expr 'U' ctl_expr ']'
  >                   | ctl_atomic_expr
  > ctl_atomic_expr   = identifier | 'next(' identifier ')'
  >                   | '(' ctl_expr ')' 
  >                   | 0 | 1

  Files may include line comments '// ...'.

======================================================================
*/

#pragma once

#include "stdio.h"
#include "arraylist.h"
#include "ctl.h"
#include "model.h"

typedef struct Property
{
   Ctl*  formula;
   char* text;
} 
Property;

typedef struct MmcFile
{
   Model*    model;
   ArrayList properties;  /* owner */
} 
MmcFile;

MmcFile mmc_read_file (FILE* file);

MmcFile mmc_read_string (const char* str);

