#include "assert.h"
#include "stdlib.h"
#include "stdio.h"
#include "ctl.h"

static Ctl* make_expr (int type)
{
   Ctl* expr = malloc(sizeof(struct Ctl));
   expr->type = type;
   expr->lhs = 0;
   expr->rhs = 0;
   expr->rc = 0;
   return expr;
}

static Ctl* make_binary_connective (int type, Ctl* lhs, Ctl* rhs)
{
   assert( lhs && rhs );
   Ctl* expr = make_expr(type);
   expr->lhs = lhs;
   expr->rhs = rhs;
   expr->lhs->rc += 1;
   expr->rhs->rc += 1;
   return expr;
}

static Ctl* make_unary_connective (int type, Ctl* e)
{
   assert( e );
   Ctl* expr = make_expr(type);
   expr->lhs = e;
   expr->lhs->rc += 1;
   return expr;
}

/* core expressions */

Ctl* ctl_EG (Ctl* e)
{
   return make_unary_connective(CTL_TEG, e);
}

Ctl* ctl_EU (Ctl* lhs, Ctl* rhs)
{
   return make_binary_connective(CTL_TEU, lhs, rhs);
}

Ctl* ctl_EX (Ctl* e)
{
   return make_unary_connective(CTL_TEX, e);
}

Ctl* ctl_not (Ctl* e)
{
   return make_unary_connective(CTL_TNOT, e);
}

Ctl* ctl_and (Ctl* lhs, Ctl* rhs)
{
   return make_binary_connective(CTL_TAND, lhs, rhs);
}

Ctl* ctl_or (Ctl* lhs, Ctl* rhs)
{
   return make_binary_connective(CTL_TOR, lhs, rhs);
}

Ctl* ctl_var (int i)
{
   assert( i >= 1 );
   Ctl* expr = make_expr(CTL_TVAR);
   expr->var = i;
   return expr;
}

Ctl* ctl_true ()
{
   return make_expr(CTL_TTRUE);
}

/* transformed expressions 

      EF p = E[1 U p]
      AG p = !EF !p
      A[p U q] = !E[!q U !p ^ !q] ^ !EG !q
      AX p = !EX !p 
      AF p = A[1 U p]

      p -> q = !p v q
      0 = !1
*/

Ctl* ctl_EF (Ctl* e)
{
   return ctl_EU(ctl_true(), e);
}

Ctl* ctl_AG (Ctl* e)
{
   return ctl_not(ctl_EF(ctl_not(e)));
}

Ctl* ctl_AU (Ctl* lhs, Ctl* rhs)
{
   return ctl_and( ctl_not(ctl_EU(ctl_not(rhs), ctl_and(ctl_not(lhs), ctl_not(rhs)))),
                   ctl_not(ctl_EG(ctl_not(rhs))));
}

Ctl* ctl_AX (Ctl* e)
{
   return ctl_not(ctl_EX(ctl_not(e)));
}

Ctl* ctl_AF (Ctl* e)
{
   return ctl_AU(ctl_true(), e);
}

Ctl* ctl_xor (Ctl* lhs, Ctl* rhs)
{
   return ctl_or(ctl_and(lhs, ctl_not(rhs)),
                 ctl_and(rhs, ctl_not(lhs)));
}

Ctl* ctl_implies (Ctl* lhs, Ctl* rhs)
{
   return ctl_or(ctl_not(lhs), rhs);
}

Ctl* ctl_equiv (Ctl* lhs, Ctl* rhs)
{
   return ctl_and(ctl_implies(lhs, rhs), ctl_implies(rhs, lhs));
}

Ctl* ctl_false ()
{
   return ctl_not(ctl_true());
}

/* destructor */

void ctl_free (Ctl* e)
{
   if (!e) return;

   if (e->rc == 0) {
      if (e->lhs) {
         e->lhs->rc -= 1; 
         ctl_free(e->lhs);
      }
      if (e->rhs) {
         e->rhs->rc -= 1; 
         ctl_free(e->rhs);
      }
      free(e);
   }
}

/* properties */

int ctl_is_propositional (Ctl* e)
{
   assert( e );
   switch (e->type) {
   case CTL_TEG: case CTL_TEU: case CTL_TEX: 
      return 0;
   case CTL_TVAR: case CTL_TTRUE: 
      return 1;
   case CTL_TNOT: 
      return ctl_is_propositional(e->lhs);
   case CTL_TAND: case CTL_TOR: 
      return ctl_is_propositional(e->lhs) && ctl_is_propositional(e->rhs);
   default:
      assert( 0 && "unknown type" );
      return 0;
   }
}

/* convert propositional formula to bdd, assumes bdd_init was called */

Bdd ctl_as_bdd (Ctl* e)
{
   Bdd bdd;

   assert( ctl_is_propositional(e) );
   switch (e->type) {
   case CTL_TVAR: 
      bdd = bdd_ithvar(e->var);
      assert( bdd && "bdd system initialized with too few variables" );
      break;
   case CTL_TTRUE: 
      bdd = bdd_true();
      break;
   case CTL_TNOT: 
      bdd = bdd_not(ctl_as_bdd(e->lhs));
      break;
   case CTL_TAND: 
      bdd = bdd_and(ctl_as_bdd(e->lhs), ctl_as_bdd(e->rhs));
      break;
   case CTL_TOR: 
      bdd = bdd_or(ctl_as_bdd(e->lhs), ctl_as_bdd(e->rhs));
      break;
   default:
      assert( 0 && "invalid type" );
   }
   return bdd;
}

/* debugging */

void ctl_print (Ctl* e)
{
   static const char* names[] = {
      "", "EG", "EU", "EX", "not", 
      "and", "or", "var", "1"
   };
   int i;

   if (!e) return;
   else if (e->type == CTL_TVAR) printf("x%d", e->var);
   else if (e->type == CTL_TTRUE) printf("1");
   else {
      printf("(%s ", names[e->type]);
      ctl_print(e->lhs);
      if (e->rhs) printf(" ");
      ctl_print(e->rhs);
      printf(")");
   }
}

