/* Dynamically sized array */

#pragma once

typedef struct ArrayListTag* ArrayList;

typedef void (*FreeFn)(void* ptr);

typedef void* T;

/* construction/destruction */ 

ArrayList arraylist_new ();

ArrayList arraylist_with_ownership (FreeFn freefn);

void arraylist_free (ArrayList a);

/* ownership */

void arraylist_release_ownership (ArrayList a);

/* accessors */

T arraylist_get (ArrayList a, int i);

T arraylist_back (ArrayList a);

int arraylist_size (ArrayList a);

int arraylist_is_empty (ArrayList a);

/* mutators */

void arraylist_set (ArrayList a, int i, T val);

void arraylist_push (ArrayList a, T elem);

T    arraylist_pop (ArrayList a);

void arraylist_resize (ArrayList a, int sz_new);

void arraylist_append (ArrayList a, ArrayList b);

void arraylist_append_data (ArrayList a, T* data, int n);

/* unsafe */

T* arraylist_data (ArrayList a);
