#include "stdlib.h"
#include "string.h"
#include "bdd.h"
#include "iter.h"
#include "model.h"

/* interleave input & output variables in the BDD ordering */

#define ivar_id(n) (1 + 2*n)
#define ovar_id(n) (2 + 2*n)

typedef struct Model
{
   Bdd        R;       /* transition relation */
   Bdd        I;       /* initial states */
   ArrayList  names;   /* names of variables */

   struct {            /* precomputed values */
     int*  ivar_id;    /* bdd index for every input variable */
     int*  ovar_id;    /* bdd index for every output variable */
     Bdd   ioequiv;    /* forall i: x_i <-> x_i' */
   } pre;
} Model;

/* construction/destruction */

Model* model_new (Bdd R, Bdd I, ArrayList names)
{
   int i, nvars;
   Model* m;

   nvars = arraylist_size(names);
   m = malloc(sizeof(Model));
   m->pre.ivar_id = malloc(sizeof(int) * nvars);
   m->pre.ovar_id = malloc(sizeof(int) * nvars);
   m->R = R;
   m->I = I;
   m->names = names;

   /* calculate variable indices */
   for (i = 0; i < nvars; ++i) {
     m->pre.ivar_id[i] = ivar_id(i); 
     m->pre.ovar_id[i] = ovar_id(i);
   }

   /* precompute i/o equivalence */
   m->pre.ioequiv = bdd_true();
   for (i = 0; i < nvars; ++i)
     m->pre.ioequiv = bdd_and(m->pre.ioequiv, 
       bdd_equiv(bdd_ithvar(ivar_id(i)), bdd_ithvar(ovar_id(i))));

   return m;
}

void model_free (Model* m)
{
   arraylist_free(m->names);
   free(m->pre.ivar_id);
   free(m->pre.ovar_id);
   free(m);
}

/* accessors */

Bdd model_relation (Model* m)
{
   return m->R;
}

Bdd model_initial_states (Model* m)
{
   return m->I;
}

const char* model_var_name (Model* m, int var)
{
   return arraylist_get(m->names, var);
}

int model_num_vars (Model* m)
{
   return arraylist_size(m->names);
}

/* quantify out multiple variables at once */

static Bdd exists_n (Bdd bdd, int* vars, int n)
{  
   int i;
   for (i = 0; i < n; ++i) 
      bdd = bdd_exists(bdd, vars[i]);
   return bdd;
}

/* transforming an input set to output set & vice versa */

static Bdd rename_in_to_out (Model* m, Bdd S)
{
   return exists_n(bdd_and(S, m->pre.ioequiv), 
         m->pre.ivar_id, model_num_vars(m));
}

static Bdd rename_out_to_in (Model* m, Bdd S)
{
   return exists_n(bdd_and(S, m->pre.ioequiv), 
         m->pre.ovar_id, model_num_vars(m));
}

/* domain & range of relations */

static Bdd domain (Model* m, Bdd R)
{
   return exists_n(R, m->pre.ovar_id, model_num_vars(m));
}

static Bdd range (Model* m, Bdd R)
{
   return rename_out_to_in(m, 
         exists_n(R, m->pre.ivar_id, model_num_vars(m)));
}

/* traversal */

Bdd model_successors (Model* m, Bdd S)
{
   return range(m, bdd_and(m->R, S));
}

Bdd model_predecessors (Model* m, Bdd S)
{
   return domain(m, bdd_and(m->R, rename_in_to_out(m, S)));
}

Bdd model_strict_predecessors (Model* m, Bdd S)
{
   return bdd_not(model_predecessors(m, bdd_not(S)));
}

Bdd model_set_to_relation (Model* m, Bdd S)
{
   return bdd_and(S, m->pre.ioequiv);
}

Bdd model_reachable (Model* m, Bdd I)
{
   Bdd bdd = I, prev;

   do {
     prev = bdd;
     bdd = bdd_or(bdd, model_successors(m, bdd));
   } while (bdd != prev);
   return bdd;
}

int model_export_dot (Model* m, FILE* out)
{
   int i, nvars = model_num_vars(m);
   Bdd bdd = model_reachable(m, m->I);
   BddIter* it;

   /* global graph settings */
   fprintf(out,
      "digraph { \n"
      "  fontsize=30; \n"
      "  bgcolor=\"#dfdfdf\"; \n"
      "  node [ shape=\"Mrecord\", style=\"filled\" ];\n");

   /* list all states */
   it = bdd_iter_new(bdd_and(bdd, m->pre.ioequiv), 
                     nvars * 2);
   while (!bdd_iter_done(it)) {
      fprintf(out, "  S");
      for (i = 0; i < nvars; ++i)
         fprintf(out, "%d", bdd_iter_value(it, ivar_id(i)));
      fprintf(out, " [ label=\"");
      for (i = 0; i < nvars; ++i)
         if (bdd_iter_value(it, ivar_id(i)))
            fprintf(out, "%s\\n", model_var_name(m, i));
      fprintf(out, "\" fillcolor=\"lightgray\" ];\n");
      bdd_iter_advance(it);
   }
   bdd_iter_free(it);

   /* list all transitions */
   it = bdd_iter_new(bdd_and(bdd, m->R), 
                     nvars * 2);
   while (!bdd_iter_done(it)) {
      fprintf(out, "  S");
      for (i = 0; i < nvars; ++i)
         fprintf(out, "%d", bdd_iter_value(it, ivar_id(i)));
      fprintf(out, " -> S");
      for (i = 0; i < nvars; ++i)
         fprintf(out, "%d", bdd_iter_value(it, ovar_id(i)));
      fprintf(out, " [ ];\n");
      bdd_iter_advance(it);
   }
   bdd_iter_free(it);

   fprintf(out, "}\n");
}
