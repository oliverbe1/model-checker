#include "assert.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "arraylist.h"
#include "parser.h"
#include "check.h"

#define ARRSIZE(x) (sizeof(x)/sizeof(x[0]))

typedef struct Destructible
{
   int is_destroyed;
} Destructible;

Destructible* destructible_new ()
{
   return calloc(1, sizeof(Destructible));
}

void destructible_free (void* o)
{
   if (o)
      ((Destructible*)o)->is_destroyed = 1;
}

void t_insertions_adjust_size ()
{
   ArrayList a, b;
   void* data[] = { (void *)3, (void *)8 };

   a = arraylist_new();
   assert( arraylist_size(a) == 0 );
   arraylist_push(a, (void *)2);
   assert( arraylist_size(a) == 1 );
   arraylist_append_data(a, data, ARRSIZE(data));
   assert( arraylist_size(a) == 3 );

   b = arraylist_new();
   arraylist_push(b, (void *)0);
   arraylist_append(a, b);
   assert( arraylist_size(a) == 4 );

   arraylist_resize(a, 100);
   assert( arraylist_size(a) == 100 );
}

void t_deletions_adjust_size ()
{
   ArrayList a;

   a = arraylist_new();

   arraylist_push(a, (void *)1);
   arraylist_push(a, (void *)2);
   arraylist_pop(a);
   assert( arraylist_size(a) == 1 );

   arraylist_push(a, (void *)2);
   arraylist_push(a, (void *)3);
   arraylist_resize(a, 1);
   assert( arraylist_size(a) == 1 );
}

void t_owned_objects_are_freed ()
{
   ArrayList a;
   Destructible *d1, *d2;

   a = arraylist_with_ownership(destructible_free);
   arraylist_push(a, d1 = destructible_new());
   arraylist_push(a, d2 = destructible_new());
   assert( !d2->is_destroyed );

   arraylist_pop(a);
   assert( d2->is_destroyed );

   arraylist_resize(a, 0);
   assert( d1->is_destroyed );

   d1 = destructible_new();
   arraylist_push(a, d1);
   arraylist_set(a, 0, d2);
   assert( d1->is_destroyed );

   d1 = destructible_new();
   arraylist_push(a, d1);
   arraylist_release_ownership(a);
   arraylist_pop(a);
   assert( !d1->is_destroyed );
}

void t_read_mmc_file ()
{
   MmcFile f;
   const char* program =
      " var MyVar_1 = 1  // some comment   \n"
      " var MyVar_2 = 0                    \n"
      "                                    \n"
      " next(MyVar_2) (+) next(MyVar_1)    \n"
      "                                    \n"
      " check AG MyVar_1                   \n"
      "                                    \n";
   f = mmc_read_string(program);
   assert( strcmp(model_var_name(f.model, 0), "MyVar_1") == 0 );
   assert( strcmp(model_var_name(f.model, 1), "MyVar_2") == 0 );
}

int main ()
{
   /* arraylist */
   t_insertions_adjust_size();
   t_deletions_adjust_size();
   t_owned_objects_are_freed();

   /* parser */
   t_read_mmc_file();

   return 0;
}
