/* Binary decision diagram iterator over satisfying assignments */

#pragma once

#include "bdd.h"

typedef struct BddIter BddIter;

/* construction/destruction */

BddIter* bdd_iter_new (Bdd bdd, int num_vars);

void bdd_iter_free (BddIter* it);

/* iteration */

int bdd_iter_advance (BddIter* it);

int bdd_iter_done (BddIter* it);

/* access assignment values */

int bdd_iter_value (BddIter* it, int var);

void bdd_iter_copy_values (BddIter* it, int* out);
