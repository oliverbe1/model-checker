#include "assert.h"
#include "stdlib.h"
#include "iter.h"
#include "check.h"

#define MAXPATHSIZE 200

/* static data */

static Bdd trail[MAXPATHSIZE];

/* path */

void path_free (Path* p)
{
   if (!p) return;
   free(p->values);
   free(p);
}

/* model checking */

static Bdd check_EG (Model* m, Ctl* e)
{
   Bdd bdd, prev;

   bdd = mmc_check(m, e);
   do {
     prev = bdd;
     bdd = bdd_and(bdd, model_predecessors(m, bdd));
   } while (bdd != prev);
   return bdd;
}

static Bdd check_EX (Model* m, Ctl* e)
{
   return model_predecessors(m, mmc_check(m, e));
}

static Bdd check_EU (Model* m, Ctl* lhs, Ctl* rhs)
{
   Bdd bdd, prev, L;

   L = mmc_check(m, lhs);
   bdd = mmc_check(m, rhs);
   do {
     prev = bdd;
     bdd = bdd_or(bdd, bdd_and(L, model_predecessors(m, bdd)));
   } while (bdd != prev);
   return bdd;
}

Bdd mmc_check (Model* m, Ctl* e)
{
   switch (e->type) {
   case CTL_TEG: 
      return check_EG(m, e->lhs);
   case CTL_TEU: 
      return check_EU(m, e->lhs, e->rhs);
   case CTL_TEX: 
      return check_EX(m, e->lhs);
   case CTL_TVAR: 
      return bdd_ithvar(e->var);
   case CTL_TTRUE: 
      return bdd_true();
   case CTL_TNOT: 
      return bdd_not(mmc_check(m, e->lhs));
   case CTL_TAND: 
      return bdd_and(mmc_check(m, e->lhs), mmc_check(m, e->rhs));
   case CTL_TOR: 
      return bdd_or(mmc_check(m, e->lhs), mmc_check(m, e->rhs));
   default:
      assert( 0 && "unknown type" );
      return 0;
   }
}

/* witness & counter example generation */

static void copy_one_state (Bdd bdd, int num_vars, int* out)
{
   int i;
   BddIter* it = bdd_iter_new(bdd, num_vars * 2);

   assert( !bdd_iter_done(it) );
   for (i = 0; i < num_vars; ++i)
      out[i] = bdd_iter_value(it, 1 + 2*i);
   bdd_iter_free(it);
}

static Path* make_path (Bdd* trail, int sz, int num_vars, int loop)
{
   int i;
   Path* p;

   p = malloc(sizeof(Path));
   p->values = malloc(sizeof(int) * num_vars * sz);
   p->num_states = sz;
   p->num_vars = num_vars;
   p->loop_start = loop;

   for (i = 0; i < sz; ++i) 
      copy_one_state(trail[i], num_vars, p->values + i*num_vars);

   return p;
}

static Bdd last_intersection (Bdd* trail, int sz, Bdd S, int* n_)
{
   int i;
   Bdd bdd;

   for (i = sz - 1; i >= 0; --i) {
      bdd = bdd_and(trail[i], S);
      if (bdd != bdd_false()) {
         *n_ = i;
         return bdd;
      }
   }
   return bdd_false();
}

Path* witness_EG (Model* m, Ctl* e)
{
   int i;
   int sz = 0;
   int loop;
   Bdd S, X, E;

   /* locate nearest cycle */
   E = mmc_check(m, e);
   S = bdd_and(model_initial_states(m), E);
   do {
      trail[sz++] = S;
      S = bdd_and(model_successors(m, S), E);
      X = last_intersection(trail, sz, S, &loop);
      if (sz >= MAXPATHSIZE) return 0;
   } while (X == bdd_false());

   /* pick single states that lead to the cycle */
   S = bdd_satone_state(X);
   for (i = sz - 1; i >= 0; --i) {
      S = bdd_and(trail[i], model_predecessors(m, S));
      S = bdd_satone_state(S);
      trail[i] = S;
   }

   return make_path(trail, sz, model_num_vars(m), loop);
}

Path* witness_EU (Model* m, Ctl* lhs, Ctl* rhs)
{
   int i;
   int sz = 0;
   Bdd S, L, R;

   /* calculate sets where lhs, rhs hold */
   L = mmc_check(m, lhs);
   R = mmc_check(m, rhs);

   /* find first reachable state where rhs holds */
   S = model_initial_states(m);
   while (bdd_and(S, R) == bdd_false()) {
      S = bdd_and(S, L);
      trail[sz++] = S;
      S = model_successors(m, S);
      if (sz >= MAXPATHSIZE) return 0;
   } 
   S = bdd_satone_state(bdd_and(S, R));
   trail[sz++] = S;

   /* pick single states that lead to the state where rhs holds */
   for (i = sz - 2; i >= 0; --i) {
      S = bdd_and(trail[i], model_predecessors(m, S));
      S = bdd_satone_state(S);
      trail[i] = S;
   }

   return make_path(trail, sz, model_num_vars(m), -1);
}

Path* witness_EX (Model* m, Ctl* e)
{
   Bdd S, E;

   E = mmc_check(m, e);
   S = bdd_satone_state(model_initial_states(m));
   trail[0] = S;
   S = bdd_and(model_successors(m, S), E);
   trail[1] = S;
   return make_path(trail, 2, model_num_vars(m), -1);
}

Path* mmc_witness (Model* m, Ctl* e)
{
   Path* p;

   switch (e->type) {
   case CTL_TEG:
      return witness_EG(m, e->lhs);
   case CTL_TEU:
      return witness_EU(m, e->lhs, e->rhs);
   case CTL_TEX:
      return witness_EX(m, e->lhs);
   case CTL_TOR:
      p = mmc_witness(m, e->lhs);
      return p ? p : mmc_witness(m, e->rhs);
   case CTL_TNOT:
      return mmc_counter_example(m, e->lhs);
   default:
      return 0;
   }
}

Path* mmc_counter_example (Model* m, Ctl* e)
{
   Path* p;

   switch (e->type) {
   case CTL_TAND:
      p = mmc_counter_example(m, e->lhs);
      return p ? p : mmc_counter_example(m, e->rhs);
   case CTL_TNOT:
      return mmc_witness(m, e->lhs);
   default:
      return 0;
   }
}
