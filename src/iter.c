#include "assert.h"
#include "string.h"
#include "stdlib.h"
#include "iter.h"

struct BddIter
{
   Bdd* restricted;  /* bdds restricted to prefix of values */
   int* values;      /* current value assignment */
   int  num_vars;    /* number of variables in bdd */
   int  cursor;      /* cursor into values/restricted arrays */
   int  done;        /* is iterator at its end */
};

static int backtrack (BddIter* it)
{
   int i;
   Bdd bdd;

   for (i = it->cursor; i >= 0; --i) {
      if (it->values[i] != 0)
         continue;
      bdd = it->restricted[i];
      bdd = bdd_restrict(bdd, i + 1, 1);
      if (bdd != bdd_false()) {
         it->values[i] = 1;
         it->cursor = i + 1;
         if (i != it->num_vars - 1)
            it->restricted[i + 1] = bdd;
         return 1;
      }
   }
   return 0;
}

static void nextsatisfying (BddIter* it)
{
   int i;
   Bdd bdd, tmp;

   for (i = it->cursor; i < it->num_vars; ++i) {
      tmp = it->restricted[i];
      bdd = bdd_restrict(tmp, i + 1, 0);
      if (bdd == bdd_false()) {
         bdd = bdd_restrict(tmp, i + 1, 1);
         it->values[i] = 1;
      } else {
         it->values[i] = 0;
      }
      if (i != it->num_vars - 1)
         it->restricted[i + 1] = bdd;
   }
   it->cursor = it->num_vars - 1;
}

/* construction/destruction */

BddIter* bdd_iter_new (Bdd bdd, int num_vars)
{
   BddIter* it;

   assert( num_vars > 0 );
   it = malloc(sizeof(BddIter));
   it->restricted = malloc(sizeof(Bdd) * num_vars);
   it->values = malloc(sizeof(int) * num_vars);
   it->num_vars = num_vars;
   it->cursor = 0;
   it->done = 0;
   it->restricted[0] = bdd;

   /* initial value assignment is (0,0,0,..) */
   memset(it->values, 0, sizeof(int) * num_vars);

   /* find first satisfying assignment */
   if (bdd == bdd_false())
      it->done = 1;
   else
      nextsatisfying(it);
   return it;
};

void bdd_iter_free (BddIter* it)
{
   free(it->restricted);
   free(it->values);
   free(it);
}

/* iteration */

int bdd_iter_advance (BddIter* it)
{
   if (it->done) 
      return 0;
   if (!backtrack(it)) {
      it->done = 1;
      return 0;
   }
   nextsatisfying(it);
   return 1;
}

int bdd_iter_done (BddIter* it)
{
   return it->done;
}

/* access assignment values */

int bdd_iter_value (BddIter* it, int var)
{
   assert( !it->done );
   assert( var >= 1 && var <= it->num_vars );
   return it->values[var - 1];
}

void bdd_iter_copy_values (BddIter* it, int* out)
{
   assert( !it->done );
   memcpy(out, it->values, sizeof(int) * it->num_vars);
}
