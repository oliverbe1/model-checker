/* Model checking, witness & counterexample generation */

#pragma once

#include "bdd.h"
#include "ctl.h"
#include "model.h"

typedef struct Path
{
   int*  values;       /* variable assignments, grouped by state */
   int   num_states;   /* number of states in path */
   int   num_vars;     /* number of variables per state */
   int   loop_start;   /* state where last state in path loops to, or -1 */
} Path;

void path_free (Path* p);

/* returns the set of states where `e` holds */

Bdd mmc_check (Model* m, Ctl* e);

/* finds a path from an initial state proving `e`, if any */

Path* mmc_witness (Model* m, Ctl* e);

/* finds a path from an initial state disproving `e`, if any */

Path* mmc_counter_example (Model* m, Ctl* e);
