#
# Python program to generate a map coloring problem for the 
# United States. Each state can have one of four colors, so 
# 2 bits/booleans per state.
# No state must have the same color as an adjacent state, and 
# we limit color changes per transition to 1 state.
#
# This was a stress test to see how far mmc can go. It can do 
# 10 states (20+ variables) in ~1-2 minutes. Full-blown
# model checkers can handle hundreds of variables and may  
# be able to solve the entire map. (of course there are much 
# better algorithms to solve this particular problem).
#

adj1 = [
 ["ca","az"],["ca","nv"],["ca","or"],["or","wa"],["wa","id"],["or","id"],
 ["nv","id"],["nv","az"],["az","ut"],["nv","ut"],["ut","id"],["ut","wy"],
 ["ut","co"],["id","mt"],["mt","wy"],["id","wy"],["wy","co"],["az","co"],
 ["nv","or"],
 #["ut","nm"],["nm","az"],["nm","co"],["mt","nd"],["mt","sd"],["nd","sd"], 
 #["wy","sd"],["wy","nb"],["nb","co"],["nb","sd"],["nb","ks"],["co","ks"],
 #["co","ok"],["ks","ok"],["tx","ok"],["nm","ok"],["nm","tx"],["nd","mn"], 
 #["sd","mn"],["sd","ia"],["mn","ia"],["nb","ia"],["nb","mo"],["mo","ia"],
 #["mo","ks"],["mo","ok"],["mo","ar"],["ok","ar"],["ar","tx"],["ar","la"],
 #["tx","la"],["mn","wi"],["ia","wi"],["wi","il"],["ia","il"],["mo","il"],
 #["il","ky"],["mo","ky"],["ar","tn"],["tn","ky"],["ar","ms"],["ms","la"],
 #["ms","tn"],["mi","wi"],["in","il"],["in","mi"],["in","ky"],["tn","al"],
 #["al","ms"],["mi","oh"],["oh","in"],["oh","ky"],["ky","va"],["ky","wv"],
 #["oh","wv"],["tn","va"],["tn","ga"],["tn","nc"],["ga","al"],["ga","nc"],
 #["oh","pa"],["al","fl"],["ga","fl"],["ga","sc"],["nc","sc"],["va","nc"],
 #["wv","va"],["md","de"],["md","wv"],["md","va"],["md","pa"],["wv","pa"],
 #["de","pa"],["de","nj"],["pa","ny"],["pa","nj"],["ny","nj"],["ny","ct"],
 #["ny","ma"],["ma","ct"],["ma","ri"],["ct","ri"],["ny","vt"],["vt","nh"],
 #["ma","vt"],["ma","nh"],["me","nh"],
]
adj2 = adj1 + [[b, a] for [a, b] in adj1]
all  = list(set(a for a, b in adj2))

def state_bit(s, b):
    return s + str(int((b & 0b01) != 0))

def next_state_neq_current(s):
    return \
        "(next(" + state_bit(s, 0) +") != " + state_bit(s, 0) + " v " + \
        "next(" + state_bit(s, 1) +") != " + state_bit(s, 1) + ")"

def next_states_neq(s1, s2):
    return \
        "  (next(" + state_bit(s1, 0) +") != next(" + state_bit(s2, 0) + ") v " + \
        "next(" + state_bit(s1, 1) +") != next(" + state_bit(s2, 1) + "))"

def current_states_neq(s1, s2):
    return \
        "  (" + state_bit(s1, 0) +" != " + state_bit(s2, 0) + " v " + \
        "" + state_bit(s1, 1) +" != " + state_bit(s2, 1) + ")"

def state_transition(s):
    res = next_state_neq_current(s) + " -> \n" 
    res += " ^ \n".join([next_states_neq(s1, s2) for s1, s2 in adj2 if s1 == s])
    return res

print("// AUTO-GENERATED - see mapcolor.py\n")

## state variables
print("// state variables\n")
for s in all:
    for i in range(2):
        print("var " + state_bit(s, i) + " = 0")

## constraints
print("\n// transition constraints\n")
for s in all:
    print(state_transition(s) + "\n")

for i in range(len(all)):
    for j in range(i+1, len(all)):
        print("!(" + next_state_neq_current(all[i]) + " ^ " + \
                     next_state_neq_current(all[j]) + ")")

## property
print("\n// property - all adjacent different color\n")
print("check EF (\n" + \
    " ^ \n".join([current_states_neq(s1, s2) for s1, s2 in adj1]) + ")")
